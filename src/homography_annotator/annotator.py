# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import os
import sys
import argparse

import json
import numpy as np
import cv2
import matplotlib
matplotlib.use("Qt5Agg")

from PyQt5 import QtWidgets, QtGui, QtCore

from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar, FigureCanvasQTAgg as FigureCanvas)
from matplotlib.figure import Figure
from matplotlib.patches import Polygon

# Personnal modules
from .utils import PolygonInteractor, mkdirs


class ResultCanvas(FigureCanvas):
    def __init__(self, src_canvas, dst_canvas, parent=None, width=5, height=4, dpi=100):
        self.display_style = 'img'
        self.show_src = True
        self.show_dst = True

        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)

        src_img = cv2.cvtColor(cv2.cvtColor(src_canvas.img, cv2.COLOR_BGR2GRAY), cv2.COLOR_GRAY2BGR)
        src_edge_img = cv2.Canny(src_img, 20, 60)
        src_edge_img = cv2.cvtColor(src_edge_img, cv2.COLOR_GRAY2BGR)
        # src_img = cv2.cvtColor(src_img, cv2.COLOR_BGR2HSV)
        # src_img[..., 0] = 161
        # src_img[..., 1] = 255
        # src_img = cv2.cvtColor(src_img, cv2.COLOR_HSV2BGR)
        self.src_img = src_img
        self.src_edge_img = src_edge_img

        dst_img = cv2.cvtColor(cv2.cvtColor(dst_canvas.img, cv2.COLOR_BGR2GRAY), cv2.COLOR_GRAY2BGR)
        dst_edge_img = cv2.Canny(dst_img, 20, 60)
        dst_edge_img = cv2.cvtColor(dst_edge_img, cv2.COLOR_GRAY2BGR)
        # dst_img = cv2.cvtColor(dst_img, cv2.COLOR_BGR2HSV)
        # dst_img[..., 0] = 0
        # dst_img[..., 1] = 255
        # dst_img = cv2.cvtColor(dst_img, cv2.COLOR_HSV2BGR)
        self.dst_img = dst_img
        self.dst_edge_img = dst_edge_img

        self.src_canvas = src_canvas
        self.dst_canvas = dst_canvas
        self.shown_img = self.axes.imshow(self.src_img[..., ::-1]) # convert BGR to RGB

        FigureCanvas.__init__(self, self.fig)

        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        # reduce the amount of surrounding whitespace
        self.fig.subplots_adjust(left=0, right=1, top=1, bottom=0, wspace=0, hspace=0)
        self.axes.axis('off')

        self.warp()

    def warp(self):
        src_pts = self.src_canvas.poly.get_xy()
        dst_pts = self.dst_canvas.poly.get_xy()

        H, status = cv2.findHomography(src_pts, dst_pts)
        self.src_pts = src_pts
        self.dst_pts = dst_pts
        self.H = H

        src_img, dst_img = self.get_images()

        warped_src = cv2.warpPerspective(src_img, H,
                                         (self.dst_img.shape[1],
                                          self.dst_img.shape[0]))

        to_show = np.zeros_like(dst_img)
        if self.show_src and self.show_dst:
            to_show = np.uint8(warped_src * 0.5 + dst_img * 0.5)
        elif self.show_src:
            to_show = warped_src
        elif self.show_dst:
            to_show = dst_img

        self.shown_img.set_data(to_show[..., ::-1])
        self.draw()

    def get_images(self):
        if self.display_style == 'img':
            return self.src_img, self.dst_img
        else:
            return self.src_edge_img, self.dst_edge_img

    def toggle_display_style(self):
        if self.display_style == 'img':
            self.display_style = 'edge'
        else:
            self.display_style = 'img'

        self.warp()

    def toggle_src(self):
        self.show_src = not self.show_src
        self.warp()

    def toggle_dst(self):
        self.show_dst = not self.show_dst
        self.warp()

class Annotator(FigureCanvas):
    """A canvas that updates itself every second with a new plot."""
    updated = QtCore.pyqtSignal()

    def __init__(self, img_path, parent=None, width=5, height=4, dpi=100):
        self.img = cv2.imread(img_path)

        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)
        self.axes.imshow(self.img[..., ::-1]) # convert BGR to RGB

        FigureCanvas.__init__(self, self.fig)

        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.poly = Polygon(([10, 0], [30, 0], [30, 40], [10, 40]), animated=True, facecolor=None, alpha=0.0)
        self.axes.add_patch(self.poly)
        self.p = PolygonInteractor(self.axes, self.poly)
        self.p.add_callback(self.update_callback)
        # reduce the amount of surrounding whitespace
        self.fig.subplots_adjust(left=0, right=1, top=1, bottom=0, wspace=0, hspace=0)
        self.axes.axis('off')

    def update_callback(self):
        self.updated.emit()

    def updateFigure(self):
        self.draw()

class Main(QtWidgets.QMainWindow):
    def __init__(self, src_path, dst_path, output=None):
        super().__init__()
        self.src_path = src_path
        self.dst_path = dst_path
        content = Content(src_path, dst_path)
        self.content = content
        self.setCentralWidget(content)
        self.setWindowTitle('Homography annotator')

        menu = self.menuBar().addMenu('&File')
        close = menu.addAction('&Close')
        close.setShortcut(QtGui.QKeySequence('q'))
        close.triggered.connect(self.close)

        self.output = output
        if output is not None:
            if os.path.exists(output):
                with open(output, 'r') as fin:
                    config = json.loads(fin.read())
                self.content.src_canvas.p.set_xy(np.array(config['src_pts_xy']))
                self.content.dst_canvas.p.set_xy(np.array(config['dst_pts_xy']))

        self.show()

    def close(self):
        H = self.content.result_canvas.H.tolist()
        src_pts = self.content.result_canvas.src_pts.tolist()
        dst_pts = self.content.result_canvas.dst_pts.tolist()

        results = {'H': H,
                   'src_pts_xy': src_pts,
                   'dst_pts_xy': dst_pts}
        json_results = json.dumps(results, sort_keys=True)

        if self.output is not None:
            mkdirs(os.path.abspath(os.path.dirname(self.output)), clean=False)
            with open(self.output, 'w') as fout:
                fout.write(json_results+'\n')
        else:
            print(json_results)
        QtWidgets.QApplication.quit()

class Content(QtWidgets.QWidget):
    def __init__(self, src_path, dst_path):
        super().__init__()
        self.src_path = src_path
        self.dst_path = dst_path
        self.initUI()

    def initUI(self):
        src_canvas = Annotator(self.src_path)
        src_toolbar = NavigationToolbar(src_canvas, self)

        dst_canvas = Annotator(self.dst_path)
        dst_toolbar = NavigationToolbar(dst_canvas, self)

        result_canvas = ResultCanvas(src_canvas, dst_canvas)
        result_toolbar = NavigationToolbar(result_canvas, self)
        src_canvas.updated.connect(result_canvas.warp)
        dst_canvas.updated.connect(result_canvas.warp)

        result_toggle_button = QtWidgets.QPushButton("Show edges")
        result_toggle_button.setCheckable(True)
        result_toggle_button.clicked.connect(result_canvas.toggle_display_style)

        src_toggle_button = QtWidgets.QPushButton("src")
        src_toggle_button.setCheckable(True)
        src_toggle_button.setChecked(True)
        src_toggle_button.clicked.connect(result_canvas.toggle_src)

        dst_toggle_button = QtWidgets.QPushButton("dst")
        dst_toggle_button.setCheckable(True)
        dst_toggle_button.setChecked(True)
        dst_toggle_button.clicked.connect(result_canvas.toggle_dst)

        layout = QtWidgets.QHBoxLayout()
        inputs_layout = QtWidgets.QVBoxLayout()

        src_layout = QtWidgets.QVBoxLayout()
        src_layout.addWidget(src_canvas)
        src_layout.addWidget(src_toolbar)

        dst_layout = QtWidgets.QVBoxLayout()
        dst_layout.addWidget(dst_canvas)
        dst_layout.addWidget(dst_toolbar)

        inputs_layout.addLayout(src_layout)
        inputs_layout.addLayout(dst_layout)

        controls_layout = QtWidgets.QHBoxLayout()
        controls_layout.addWidget(src_toggle_button)
        controls_layout.addWidget(dst_toggle_button)
        controls_layout.addWidget(result_toggle_button)

        result_layout = QtWidgets.QVBoxLayout()
        result_layout.addWidget(result_canvas)
        result_layout.addWidget(result_toolbar)
        result_layout.addLayout(controls_layout)

        layout.addLayout(inputs_layout, 33)
        layout.addLayout(result_layout, 67)

        self.setLayout(layout)

        self.result_canvas = result_canvas
        self.src_canvas = src_canvas
        self.dst_canvas = dst_canvas

def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--src', help='path to source image', required=True)
    parser.add_argument('--dst', help='path to target image', required=True)
    parser.add_argument('--output', help='path to input/output json file')

    return parser.parse_args()

def run(args):
    app = QtWidgets.QApplication(sys.argv)
    window = Main(args.src, args.dst, args.output)
    return app.exec_()

    return 0

def main():
    args = parse_arguments()
    return run(args)

if __name__ == '__main__':
    sys.exit(main())
