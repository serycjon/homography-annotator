* Homography annotator
Simple tool for precise annotation of homography between two images.

** Installation
Install the tool by running
#+BEGIN_SRC sh :exports code
pip install -e .
#+END_SRC

** Usage
#+BEGIN_SRC sh :exports code
h_annotator --src path/to/img_a --dst path/to/img_b
#+END_SRC
When done with annotation, press =q=.  This will quit the tool and print json containing the homography and the clicked points positions in both images to the standard output.  You can also provide an optional parameter =--output path/to/output.json=, in which case the tool first tries to initialize from the provided tool and on exit it saves the annotation into the output file instead of printing it to the stdout.

Press /Show edges/ to show the alignment using edge images.  You can also toggle the display of each of the images in the result window using the /src/ and /dst/ buttons.
