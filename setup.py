from setuptools import setup, find_packages

setup(
    name='homography_annotator',
    version='0.1',
    description='Annotate single homography between two images',
    author='Jonas Serych',
    author_email='jonas.serych@gmail.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'opencv-python>=3,<4',
        'numpy>=1.14.5',
        'matplotlib>=2.2.3',
        'pyqt5'
        ],
    entry_points={
        'console_scripts': [
            'h_annotator = homography_annotator.annotator:main'
        ]},
    )
